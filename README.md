# lambda-dynamodb-websockets
This project is to demo the use of DynamoDB and a Websocket API Gateway to
communicate changes of items to all connected clients via a websocket.

## Build Packages
```bash
./gradlew build
```

## Terraform Deploy
**Note:** Before deployment, the WebSocket API Gateway should be created manually and deployed.
The stage url will be used as input to the terraform deployment.

```bash
export AWS_PROFILE=profile-name
export AWS_DEFAULT_REGION=us-east-1
terraform apply 
```

**Note:** After deployment, the WebSocket API Gateway routes should be set manually.
- The **websocket-connection-listener** should be added to the connect and
    disconnect routes
- The **node-simple-logger** can be added to the default route
