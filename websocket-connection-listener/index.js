const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB({apiVersion: '2012-08-10'});
const ttlSeconds = process.env['TTL_SECONDS'] || "9000";
const tableName = process.env['TABLE_NAME'] || 'connection';

exports.handler = async (event) => {
    console.log(JSON.stringify(event));
    const isConnect = event.requestContext && event.requestContext.eventType === 'CONNECT' || false;
    const isDisconnect = event.requestContext && event.requestContext.eventType === 'DISCONNECT' || false;
    const connectionId = event.requestContext && event.requestContext.connectionId || null;
    const domainName = event.requestContext && event.requestContext.domainName || null;
    const stage = event.requestContext && event.requestContext.stage || null;

    if (isConnect) {
        await addConnection(connectionId, domainName, stage)
            .then(response => console.log("addConnection", JSON.stringify(response)))
            .catch(error => console.log("addConnection", JSON.stringify(error)));
    } else if (isDisconnect) {
        await removeConnection(connectionId)
            .then(response => console.log("removeConnection", JSON.stringify(response)))
            .catch(error => console.log("removeConnection", JSON.stringify(error)));
    }

    return {};
};

async function addConnection(connectionId, domainName, stage) {
    return dynamoDB.putItem({
        TableName: tableName,
        Item: {
            connection_id: {
                S: connectionId
            },
            ttl: {
                S: `${getTtlEpochTime()}`
            },
            domain_name: {
                S: domainName
            },
            stage: {
                S: stage
            }
        }
    }).promise()
}

async function removeConnection(connectionId) {
    return dynamoDB.deleteItem({
        TableName: tableName,
        Key: {
            connection_id: {
                S: connectionId
            }
        }
    }).promise()
}

function getTtlEpochTime() {
    return getEpochTime() + parseInt(ttlSeconds, 10);
}

function getEpochTime() {
    return Math.ceil(new Date().getTime() / 1000);
}