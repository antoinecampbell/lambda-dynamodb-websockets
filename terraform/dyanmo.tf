resource "aws_dynamodb_table" "websocket_connections" {
  name         = "connection"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "connection_id"

  attribute {
    name = "connection_id"
    type = "S"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }
}

resource "aws_dynamodb_table" "item" {
  name         = "item"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "item_id"

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "item_id"
    type = "S"
  }
}