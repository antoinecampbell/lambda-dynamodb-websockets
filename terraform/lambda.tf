locals {
  environment_variables = {
    STUB = ""
  }
}

module "node_simple_logger" {
  source = "./modules/lambda-vpc-log-group"

  lambda_zip_location = "../node-simple-logger/build/distributions/node-simple-logger.zip"

  function_name = "ac-node-simple-logger"
  role_arn      = aws_iam_role.lambda_exec.arn
  runtime       = "nodejs12.x"
  handler       = "index.handler"

  security_group_ids = []
  subnet_ids         = []

  environment_variables = local.environment_variables
}

module "websocket_connection_listener" {
  source = "./modules/lambda-vpc-log-group"

  lambda_zip_location = "../websocket-connection-listener/build/distributions/websocket-connection-listener.zip"

  function_name = "ac-websocket-connection-listener"
  role_arn      = aws_iam_role.lambda_exec.arn
  runtime       = "nodejs12.x"
  handler       = "index.handler"

  security_group_ids = []
  subnet_ids         = []

  environment_variables = {
    TABLE_NAME  = aws_dynamodb_table.websocket_connections.name
    TTL_SECONDS = 9000
  }
}

module "dynamodb_listener" {
  source = "./modules/lambda-vpc-log-group"

  lambda_zip_location = "../dynamodb-listener/build/distributions/dynamodb-listener.zip"

  function_name = "ac-dynamodb-listener"
  role_arn      = aws_iam_role.lambda_exec.arn
  runtime       = "nodejs12.x"
  handler       = "index.handler"

  security_group_ids = []
  subnet_ids         = []

  environment_variables = local.environment_variables

  reserved_concurrent_executions = 1
}

resource "aws_lambda_event_source_mapping" "dynamodb_listener" {
  event_source_arn  = aws_dynamodb_table.item.stream_arn
  function_name     = module.dynamodb_listener.function_name
  starting_position = "LATEST"
  batch_size        = 1
}