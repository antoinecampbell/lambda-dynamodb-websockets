variable "function_name" {}
variable "role_arn" {}
variable "runtime" {}
variable "handler" {}
variable "lambda_zip_location" {}
variable "timeout" {
  default = 15
}
variable "memory_size" {
  default = 128
}
variable "reserved_concurrent_executions" {
  default = -1
}
variable "security_group_ids" {
  type = "list"
}
variable "subnet_ids" {
  type = "list"
}
variable "environment_variables" {
  type = "map"
}
variable "log_group_retention_in_days" {
  default = 14
}

resource "aws_lambda_function" "get_items" {
  function_name = var.function_name
  role          = var.role_arn
  runtime       = var.runtime

  handler          = var.handler
  filename         = var.lambda_zip_location
  source_code_hash = filebase64sha256(var.lambda_zip_location)

  timeout     = var.timeout
  memory_size = var.memory_size

  reserved_concurrent_executions = var.reserved_concurrent_executions

  vpc_config {
    security_group_ids = var.security_group_ids
    subnet_ids         = var.subnet_ids
  }

  environment {
    variables = var.environment_variables
  }
}

resource "aws_cloudwatch_log_group" "get_item" {
  name = "/aws/lambda/${var.function_name}"

  retention_in_days = var.log_group_retention_in_days
}

output "function_name" {
  value = var.function_name
}