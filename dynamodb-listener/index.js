const AWS = require('aws-sdk');

const dynamoDB = new AWS.DynamoDB({apiVersion: '2012-08-10'});
const tableName = process.env['TABLE_NAME'] || 'connection';

exports.handler = async (event) => {
    console.log(JSON.stringify(event));
    try {
        const connections = await getAllConnections()
            .then(response => {
                console.log(JSON.stringify(response));
                return response;
            })
            .catch(error => {
                console.log(JSON.stringify(error));
                throw new Error("Error fetching items");
            });
        for (const record of event.Records) {
            for (const connection of connections.Items) {
                const connectionId = getConnectionId(connection);
                const apiGatewayManagementApi = new AWS.ApiGatewayManagementApi({
                    apiVersion: '2018-11-29',
                    endpoint: getEndpoint(connection)
                });
                await apiGatewayManagementApi.postToConnection({
                    ConnectionId: connectionId,
                    Data: JSON.stringify(record)
                }).promise()
                    .then(response => console.log(JSON.stringify(response)))
                    .catch(error => console.log(JSON.stringify(error)));
            }
        }
    } catch (e) {
        console.error(e);
        console.error(JSON.stringify(e));
    }

    return {};
};

async function getAllConnections() {
    return dynamoDB.scan({
        TableName: tableName
    }).promise();
}

function getConnectionId(connection) {
    return connection && connection['connection_id'] && connection['connection_id']['S'] || null;
}

function getEndpoint(connection) {
    return `https://${connection['domain_name']['S']}/${connection['stage']['S']}`
}